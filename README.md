# Introduction

This is a very simple I2C driver to manage devices connected to the core in M5Stack boards.

# Usage

In esp-idf or ESP8266\_RTOS\_SDK menuconfig, go to `Component config -> M5Stack core I2C bus configuration`. There you can set the GPIO pins used for SCL and SDA, the I2C port number and the bus frequency. Once configured, in your code call `ci2c_init()` once to initialize the bus, and then `ci2c_read()` to read data from the bus and `ci2c_write()` to write data. Note that for the `ci2c_read()` and `ci2c_write()` functions, you will have to supply the interface, containing the device I2C address. This has been done like this for the driver to be compatible with the read/write callbacks defined by the [Bosch Sensortec BMM150 driver](https://github.com/BoschSensortec/BMM150-Sensor-API).

# License

This program is provided with NO WARRANTY, under the [Mozilla Public License (MPL)](https://www.mozilla.org/en-US/MPL/2.0/). I am not responsible in any way if the code does not work, or causes any harm. Make sure you read the warning above in this regard.

