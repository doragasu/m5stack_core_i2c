#include <driver/i2c.h>

#include <core_i2c.h>

// TODO: API is not thread safe. To make API thread safe, allow optionally
// enabling the function in Kconfig, then wrap the calls to
// i2c_master_cmd_begin() inside a mutex.

#define I2C_PORT CONFIG_CORE_I2C_PORT_NUMBER
#define I2C_SCL_PIN CONFIG_CORE_SCL_GPIO_PIN
#define I2C_SDA_PIN CONFIG_CORE_SDA_GPIO_PIN
#define I2C_FREQ CONFIG_CORE_I2C_FREQ

#define MS_TO_TICKS(ms) ((ms)/portTICK_RATE_MS)
#define vTaskDelayMs(ms) vTaskDelay(MS_TO_TICKS(ms))

static i2c_cmd_handle_t i2c_read_prepare(uint8_t dev_addr, uint8_t reg_addr)
{
	i2c_cmd_handle_t cmd = i2c_cmd_link_create();
	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, (dev_addr<<1) | I2C_MASTER_WRITE, true);
	i2c_master_write_byte(cmd, reg_addr, true);
	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, (dev_addr<<1) | I2C_MASTER_READ, true);

	return cmd;
}

static i2c_cmd_handle_t i2c_write_prepare(uint8_t dev_addr, uint8_t reg_addr)
{
	i2c_cmd_handle_t cmd = i2c_cmd_link_create();
	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, (dev_addr<<1) | I2C_MASTER_WRITE, true);
	i2c_master_write_byte(cmd, reg_addr, true);

	return cmd;
}

static esp_err_t i2c_perform(i2c_cmd_handle_t cmd)
{
	i2c_master_stop(cmd);
	esp_err_t err = i2c_master_cmd_begin(I2C_PORT, cmd, MS_TO_TICKS(1000));
	i2c_cmd_link_delete(cmd);

	return err;
}

esp_err_t ci2c_read(uint8_t reg_addr, uint8_t *reg_data,
		uint32_t length, struct ci2c_intf *intf)
{
	int i;
	i2c_cmd_handle_t cmd = i2c_read_prepare(intf->dev_addr, reg_addr);

	for (i = 0; i < (length - 1); i++) {
		i2c_master_read_byte(cmd, reg_data + i, 0);
	}
	// Do not ack the last byte
	i2c_master_read_byte(cmd, reg_data + i, 1);
	esp_err_t err = i2c_perform(cmd);

	return err;
}

esp_err_t ci2c_write(uint8_t reg_addr, const uint8_t *reg_data,
		uint32_t length, struct ci2c_intf *intf)
{
	i2c_cmd_handle_t cmd = i2c_write_prepare(intf->dev_addr, reg_addr);
	for (int i = 0; i < length; i++) {
		i2c_master_write_byte(cmd, reg_data[i], true);
	}
	esp_err_t err = i2c_perform(cmd);

	return err;
}

int ci2c_init(void)
{
	esp_err_t err;

	const i2c_config_t cfg = {
		.mode = I2C_MODE_MASTER,
		.scl_io_num = I2C_SCL_PIN,
		.scl_pullup_en = GPIO_PULLUP_ENABLE,
		.sda_io_num = I2C_SDA_PIN,
		.sda_pullup_en = GPIO_PULLUP_ENABLE,
		.master.clk_speed = I2C_FREQ
	};

	err = i2c_driver_install(I2C_PORT, cfg.mode, 0, 0, 0);
	if (ESP_OK == err) {
		err = i2c_param_config(I2C_PORT, &cfg);
	}

	return err;
}
