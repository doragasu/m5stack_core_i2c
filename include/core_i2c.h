#ifndef __CORE_I2C_H__
#define __CORE_I2C_H__

#include <esp_system.h>
#include <stdint.h>

struct ci2c_intf {
	uint8_t dev_addr;
};

esp_err_t ci2c_init(void);

esp_err_t ci2c_read(uint8_t reg_addr, uint8_t *reg_data,
		uint32_t length, struct ci2c_intf *intf);
esp_err_t ci2c_write(uint8_t reg_addr, const uint8_t *reg_data,
		uint32_t length, struct ci2c_intf *intf);

#endif /*__CORE_I2C_H__*/

